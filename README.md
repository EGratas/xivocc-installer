# xivocc-installer

This repository contains many debian packages providing configuration to the XiVO CC, XiVO PBX, XiVO MDS and/or XiVO UC VMs.
The main package is xivocc-installer. It is deployed on a XiVOCC machine and contains various configuration scripts executed on its install and upgrades.

## SSH Keys
On your future Xivocc machine, you must have a ssh key that knows the XiVO PBX vm. The key's name or type does not matter anymore.
```bash
ssh-keygen -t rsa -P "" -f ~/.ssh/xivocc_rsa
ssh-copy-id -i ~/.ssh/xivocc_rsa root@${XIVO_HOST}
```
any further ssh commands must use the key silently
```
ssh root@${XIVO_HOST} 'echo "hello $(uname -n)"' | cat
```

must not trigger any error or cli prompt.

## Manual Test

To test xivocc-installer before building it on dev, main package of this repository, you need to clone the `xivo-installer-script` and the `xivocc-installer-helper` projects.

inside the file  `xivo-install-script/xivocc_install.sh` comment the lines :

```bash
install_xivocc() {
...

${download} xivocc-installer <== COMMENT
${install} xivocc-installer  <== COMMENT

...
}
```

Then run the `xivo-install-script/xivocc_install.sh` to install the version of your choice (with -d or -a depending of what you want to build)

```bash
 xivo-install-script/xivocc_install.sh -a VERSION-NUMBER
```

Build manually and install the `xivocc-installer-helper`.
Build manually and install this package.

## Silent installation Test

```bash
export DEBIAN_FRONTEND=noninteractive && dpkg -i xivocc-installer_NUMBER
```
Silent install requires your custom.env to contains the prompted information

```bash
echo "XIVO_HOST=<IP ADDRESS OF THE XIVO>
XUC_HOST=<IP ADDRESS OF THE XIVOCC>
RESTART_REPLY=true
CONFIGURE_REPLY=true
WEEKS_TO_KEEP=<Number of weeks to keep>
RECORDING_WEEKS_TO_KEEP=<Number of recording weeks to keep (higher)>" > custom.env
```

## Anticipated install

To gain time, you can install XiVO CC and the XiVO in parallel.

To do so, you must decline restarting the XiVOPBX when prompted, OR providing the following custom.env for the silent install : 
```bash
echo "XIVO_HOST=<IP ADDRESS OF THE XIVO>
XUC_HOST=<IP ADDRESS OF THE XIVOCC>
RESTART_REPLY=false
CONFIGURE_REPLY=false
WEEKS_TO_KEEP=<Number of weeks to keep>
RECORDING_WEEKS_TO_KEEP=<Number of recording weeks to keep (higher)>" > /etc/docker/compose/custom.env
```

Once both XiVO (the wizard is passed) and XiVOCC (the installation finished) are ready, this script to run finishes their common configuration, on XiVOCC :
```bash
configure-pbx
```