#!/usr/bin/env bash

if [ -z "$1" ]; then
	exit 1
fi

container=$1
retries=60

for i in $(seq $retries -1 1); do
    if /usr/bin/xivo-container-healthcheck.sh "${container}"; then
        exit 0
    fi

    sleep 1
done

echo "ERROR: ${container} container is still not healthy after ${retries}s"
exit 1
