version: "3.7"

services:
  db:
    image: "xivoxc/xivo-db:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
    - 5432:5432

    environment:
    - POSTGRES_PASSWORD=proformatique
    - IS_MDS
    - USM_EVENT_SECRET
    - USM_CONFIG_SECRET
    - USM_COLLECTOR_SECRET
    - FLUENTD_SECRET
    - GRAFANA_SECRET

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /etc/passwd:/etc/passwd:ro
    - /etc/group:/etc/group:ro
    - /var/log/postgresql:/var/log/postgresql
    - /var/lib/postgresql/15:/var/lib/postgresql/15
    - /run/postgresql:/run/postgresql

    restart: always

  agid:
    image: "xivoxc/xivo-agid:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
    - 127.0.0.1:4573:4573

    extra_hosts:
    - xivo:host-gateway

    environment:
    - PLAY_AUTH_TOKEN

    volumes:
    - /var/lib/xivo-auth-keys/xivo-agid-key.yml:/var/lib/xivo-auth-keys/xivo-agid-key.yml
    - /etc/xivo/asterisk/:/etc/xivo/asterisk/
    - /var/log/xivo-agid:/var/log/
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /etc/docker/xivo/custom.env:/etc/docker/xivo/custom.env:ro
    - /var/spool/asterisk/:/var/spool/asterisk

    tmpfs:
    - /var/run/xivo-agid

    restart: always

  confgend:
    image: "xivoxc/xivo-confgend:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
    - 127.0.0.1:8669:8669

    environment:
    - IS_MDS=False
    - STUN_SERVER_ENABLE=${STUN_SERVER_ENABLE:-False}
    - TURN_SERVER_ENABLE=${TURN_SERVER_ENABLE:-True}

    volumes:
    - /var/log/xivo-confgend:/var/log/
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /etc/xivo/asterisk:/etc/xivo/asterisk:ro

    tmpfs:
    - /var/run/xivo-confgend

    restart: always

  rabbitmq:
    image: "xivoxc/rabbitmq:${XIVOCC_TAG}.${XIVOCC_DIST}"

    network_mode: "host"

    environment:
    - RABBITMQ_CONFIG_FILE=/etc/rabbitmq/xivo/rabbitmq
    - USM_EVENT_AMQP_SECRET

    volumes:    
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro

    restart: always

  config_mgt:
    image: "xivoxc/config-mgt:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
    - 9100:9000

    environment:
    - JAVA_OPTS=-Xms64m -Xmx128m
    - XIVO_HOST
    - DB_HOST
    - XIVO_CONFD_AUTH_TOKEN
    - APPLICATION_SECRET
    - QUEUEDISSUASION_DEFAULT_QUEUE
    - PLAY_AUTH_TOKEN
    - CONFIGMGT_AUTH_SECRET
    - MEETINGROOM_AUTH_DOMAIN
    - MEETINGROOM_AUTH_APP_ID

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /var/log/xivo-configmgt:/opt/docker/var/log/config-mgt
    - /var/lib/xivo/sounds/playback:/var/lib/xivo/sounds/playback:ro
    - /etc/xivo:/etc/xivo:ro

    restart: always

  outcall:
    image: "xivoxc/xivo-outcall:${XIVOCC_TAG}.${XIVOCC_DIST}"

    environment:
    - JAVA_OPTS=-Xms64m -Xmx128m
    - ARI_USERNAME
    - ARI_PASSWORD

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /var/log/xivo-outcall:/var/log/xivo-outcall

    extra_hosts:
    - asterisk:host-gateway

    restart: always

  nginx:
    image: "xivoxc/xivo-nginx:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
    - 80:80
    - 443:443
    - 8443:8443

    environment:
    - DOCKER_NET

    extra_hosts:
    - xivohost:${XIVO_HOST}

    volumes:
    - /usr/share/xivo/:/usr/share/xivo/
    - /etc/docker/nginx/ssl:/etc/nginx/ssl
    - /usr/share/xivo-certs/:/usr/share/xivo-certs/
    - /var/backups/xivo/:/var/backups/xivo/
    - /var/log/nginx:/var/log/nginx
    - webivol:/usr/share/xivo-web-interface
    - /var/www/munin/localdomain/localhost.localdomain:/var/www/munin/localdomain/localhost.localdomain

    restart: always

  webi:
    image: "xivoxc/xivo-webi:${XIVOCC_TAG}.${XIVOCC_DIST}"

    environment:
    - XIVO_UUID=${XIVO_UUID}
    - DOCKER_NET=${DOCKER_NET:-172.18.1.1}

    ports:
    - 9090:9090

    extra_hosts:
    - xivohost:${XIVO_HOST}

    volumes:
    - /etc/asterisk/extensions_extra.d/:/etc/asterisk/extensions_extra.d/
    - /etc/passwd:/etc/passwd:ro
    - /etc/group:/etc/group:ro
    - /etc/xivo/datastorage.conf:/etc/xivo/datastorage.conf
    - /etc/xivo/provider-configuration/:/etc/xivo/provider-configuration/
    - /usr/share/xivo/:/usr/share/xivo/
    - /var/backups/xivo/:/var/backups/xivo/
    - /var/log/asterisk/:/var/log/asterisk/
    - /var/log/xivo-web-interface/:/var/log/xivo-web-interface/
    - /var/lib/asterisk/moh/:/var/lib/asterisk/moh/
    - /var/lib/asterisk/sounds/:/var/lib/asterisk/sounds/
    - /var/lib/xivo/moh/:/var/lib/xivo/moh
    - /var/lib/xivo/sounds/:/var/lib/xivo/sounds/
    - /var/spool/asterisk/fax/:/var/spool/asterisk/fax/
    - /var/spool/asterisk/monitor/:/var/spool/asterisk/monitor/
    - webivol:/usr/share/xivo-web-interface
    - /var/www/munin/localdomain/localhost.localdomain:/var/www/munin/localdomain/localhost.localdomain
    - /sys/class/net:/sys/class/net:ro
    - /sys/devices:/sys/devices:ro

    restart: always

  switchboard_reports:
    image: "xivoxc/xivo-switchboard-reports:${XIVOCC_TAG}.${XIVOCC_DIST}"

    ports:
      - "127.0.0.1:9510:9510"

    environment:
      - JAVA_OPTS=-Xms64m -Xmx128m
      - REPORTING_DB_HOST
      - REPORTING_DB_PORT
      - REPORTING_DATABASE_NAME

    volumes:
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
      - /var/log/xivo-switchboard-reports:/var/log/xivo-switchboard-reports

    restart: always

  ctid:
    image: "xivoxc/xivo-ctid:${XIVOCC_TAG}.${XIVOCC_DIST}"

    network_mode: host

    environment:
      - XIVO_UUID=${XIVO_UUID}

    volumes:
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
      - /var/lib/xivo-auth-keys:/var/lib/xivo-auth-keys:ro
      - /var/lib/consul:/var/lib/consul:ro
      - /usr/share/xivo-certs:/usr/share/xivo-certs:ro
      - /var/log/xivo-ctid:/var/log/
      - /var/spool/asterisk/:/var/spool/asterisk/
    tmpfs:
      - /var/run/xivo-ctid

    restart: always
    
  usage_writer:
    image: "xivoxc/xivo-usage-writer:${XIVOCC_TAG}.${XIVOCC_DIST}"

    depends_on:
    - "rabbitmq"

    environment:
    - XIVO_UUID
    - DB_HOST
    - XIVO_HOST
    - USM_EVENT_SECRET
    - USM_CONFIG_SECRET
    - USM_EVENT_AMQP_SECRET
    - USM_EVENT_INTERVAL=${USM_EVENT_INTERVAL:-5m}
    - USM_CONFIG_INTERVAL=${USM_CONFIG_INTERVAL:-24h}

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro

    restart: always

  usage_collector:
    image: "xivoxc/xivo-usage-collector:${XIVOCC_TAG}.${XIVOCC_DIST}"

    environment:
    - XIVO_UUID
    - DB_HOST
    - XIVO_HOST
    - USM_COLLECTOR_SECRET
    - USM_COLLECT_INTERVAL=${USM_COLLECT_INTERVAL:-24h}
    - USM_BACKEND_URL
    - USM_BACKEND_TOKEN

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro

    restart: always
    
  proxy:
    image: "xivoxc/xivo-proxy:${XIVOCC_TAG}.${XIVOCC_DIST}"

    network_mode: host

    environment:
    - XIVO_HOST

    restart: always

networks:
  default:
    ipam:
      driver: default
      config:
      - subnet: ${DOCKER_NET}

volumes:
  webivol:
