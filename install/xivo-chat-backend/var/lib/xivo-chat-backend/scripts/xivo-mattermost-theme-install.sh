#!/bin/bash

set -e

COMPOSE_PATH=/etc/docker/compose/
CUSTOM_ENV_FILE=custom.env

MM_THEME_PATH=/var/lib/xivo-chat-backend/xivo-theme/
MM_DATA_PATH=/var/lib/mattermost/data/
MM_BRAND_FOLDER=brand
MM_CONFIG_PATH=/var/lib/mattermost/config/
MM_CONFIG_FILE=config.json
XIVO_THEME='{"awayIndicator":"#ef2335","buttonBg":"#ef7623","buttonColor":"#FFFFFF","centerChannelBg":"#ffffff","centerChannelColor":"#3C3C3B","codeTheme":"github","errorTextColor":"#f41212","linkColor":"#ef7623","mentionBj":"#EF9123","mentionColor":"#ffffff","mentionHighlightBg":"#fff2bb","mentionHighlightLink":"#EF7623","newMessageSeparator":"#EF9123","onlineIndicator":"#00be19","sidebarBg":"#3c3c3c","sidebarHeaderBg":"#ef7623","sidebarHeaderTextColor":"#ffffff","sidebarText":"#ffffff","sidebarTextActiveBorder":"#ef7623","sidebarTextActiveColor":"#efae23","sidebarTextHoverBg":"#ef7623","sidebarUnreadText":"#efae23"}'

get_mmuser_password() {
    if [ -f ${COMPOSE_PATH}/${CUSTOM_ENV_FILE} ]; then
        MM_USER_PWD=$(grep -oP -m 1 '^\s*MMUSER_DB_PASSWORD=\K.*' ${COMPOSE_PATH}/${CUSTOM_ENV_FILE})
    else
        MM_USER_PWD=$(whiptail --title "MMUSER DB Password not found" --inputbox "Enter the Mattermost DB user password: " 8 50 3>&1 1>&2 2>&3)
    fi
}

get_xuc_host() {
    if [ -f ${COMPOSE_PATH}/${CUSTOM_ENV_FILE} ]; then
        XUC_HOST=$(grep -oP -m 1 'XUC_HOST=\K.*' ${COMPOSE_PATH}/${CUSTOM_ENV_FILE})
    else
        XUC_HOST=$(whiptail --title "XUC server IP address not found" --inputbox "Enter this machine's external IP address: " 8 50 3>&1 1>&2 2>&3)
    fi
}


_run_psql() {
  PGPASSWORD=${MM_USER_PWD} psql -h ${XUC_HOST} -p 5443 -U mmuser mattermost -t -c "$1"
}

add_theme_to_existing_users() {
  echo "Add theme to existing users..."
  _run_psql "CREATE OR REPLACE FUNCTION theme_setter_func() RETURNS void AS \$func\$
    DECLARE
      temprow RECORD;
    BEGIN
      FOR temprow IN
        SELECT id,username FROM users
      LOOP
        IF (SELECT COUNT(*) FROM preferences WHERE userid=temprow.id AND category='theme') = 0 THEN
          INSERT INTO preferences (userid, category, name, value)
            VALUES (temprow.id, 'theme', '', '$XIVO_THEME');
        END IF;
      END LOOP;
    END
    \$func\$ LANGUAGE plpgsql;"

  _run_psql 'SELECT theme_setter_func();'
  _run_psql 'DROP FUNCTION theme_setter_func();'
}

add_theme_to_future_users() {
  echo "Add theme for future users..."
  _run_psql "CREATE OR REPLACE FUNCTION apply_default_theme_for_new_user() RETURNS TRIGGER AS \$defaultTheme\$
    BEGIN
    IF (SELECT COUNT(*) FROM preferences WHERE userid=NEW.id AND category='theme') = 0 THEN
      INSERT INTO preferences (userid, category, name, value)
      VALUES (NEW.id, 'theme', '', '$XIVO_THEME');
    END IF;
    RETURN NEW;
    END;
    \$defaultTheme\$ LANGUAGE 'plpgsql';"

  _run_psql "DROP TRIGGER IF EXISTS apply_default_theme_for_new_user ON users";
  _run_psql "CREATE TRIGGER apply_default_theme_for_new_user AFTER INSERT ON users FOR EACH ROW EXECUTE PROCEDURE apply_default_theme_for_new_user();"
}

change_login_page() {
  echo "Edit login page..."
  sed -i '/SiteName/c\   \t\"SiteName\" : \"XiVO\",' ${MM_CONFIG_PATH}${MM_CONFIG_FILE}
  sed -i '/EnableCustomBrand/c\   \t\"EnableCustomBrand\" : true,' ${MM_CONFIG_PATH}${MM_CONFIG_FILE}
  sed -i '/CustomDescriptionText/c\   \t\"CustomDescriptionText\" : \"Enterprise collaborative chat\",' ${MM_CONFIG_PATH}${MM_CONFIG_FILE}
  if [ ! -d "${MM_DATA_PATH}${MM_BRAND_FOLDER}" ]; then
    mkdir -p "${MM_DATA_PATH}${MM_BRAND_FOLDER}"
  fi
  cp -R ${MM_THEME_PATH}/* ${MM_DATA_PATH}${MM_BRAND_FOLDER}/
}

main() {
  echo -e "\e[1;33mInstallation of XiVO Mattermost Theme...\e[0m"
  get_mmuser_password
  get_xuc_host
  add_theme_to_existing_users
  add_theme_to_future_users
  change_login_page
  echo -e "\e[1;33mInstallation Done. Please restart Mattermost service for applying new login page.\e[0m"
}

main "${@}"

exit 0
