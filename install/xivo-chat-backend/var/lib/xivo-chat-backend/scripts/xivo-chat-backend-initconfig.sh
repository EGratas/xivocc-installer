#!/bin/bash

set -e

COMPOSE_PATH="/etc/docker/compose"
CUSTOM_ENV_FILE="custom.env"
MM_DB_SCRIPT_PATH="/var/lib/xivo-chat-backend/scripts/"
MM_DB_CREATION_SCRIPT="mattermost-create-db.sh"
DB_HOST="localhost"
DB_PORT="5432"

is_xivoucaddon() {
    [ -f /var/lib/xivo/uc_enabled ]
}

is_monoserversetup() {
  local result

  result=$(docker ps -aq --filter "name=xivocc_pgxivocc_1")
  [ -n "$result" ]
}

set_custom_env_parameter() {
  local PARAMETER_NAME=$1
  local PARAMETER_VALUE=$2
  sed -i "/^${PARAMETER_NAME}.*$/d" ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
  echo "${PARAMETER_NAME}"="${PARAMETER_VALUE}" >>${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

create_mmuser_db_password() {
  MMUSER_DB_PASSWORD=$(tr </dev/urandom -dc A-Za-z0-9 | head -c11)
}

create_mmadmin_password() {
  MMADMIN_PASSWORD=$(tr </dev/urandom -dc A-Za-z0-9 | head -c11)
}

get_mmadmin_password_and_set_it_in_custom_env() {
  MMADMIN_PASSWORD=$(grep -oP -m 1 '^\s*MMADMIN_PASSWORD=\K.*' ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}) || true
  if [ -z "$MMADMIN_PASSWORD" ]; then
    create_mmadmin_password
    set_custom_env_parameter "MMADMIN_PASSWORD" "${MMADMIN_PASSWORD}"
  fi
}

set_mmuser_db_secret_custom_env_file() {
  create_mmuser_db_password
  set_custom_env_parameter "MMUSER_DB_PASSWORD" "${MMUSER_DB_PASSWORD}"
  get_mmadmin_password_and_set_it_in_custom_env
  set_custom_env_parameter "DB_HOST" "db"
  set_custom_env_parameter "DB_PORT_NUMBER" "${DB_PORT}"

  if is_xivoucaddon; then
    set_custom_env_parameter "MM_DB_HOST" "\${XIVO_HOST}"
  elif is_monoserversetup; then
    set_custom_env_parameter "MM_DB_HOST" "pgxivocc"
  else
    set_custom_env_parameter "MM_DB_HOST" "${DB_HOST}"
  fi
}

setup_mm_environment() {
  if [ ! "$(getent group mattermost)" ]; then
    addgroup --gid 2000 mattermost
  fi

  if [ ! "$(getent passwd mattermost)" ]; then
    adduser --system --uid 2000 --gid 2000 --disabled-password --disabled-login --no-create-home mattermost
  fi

  MATTERMOST_BASE_DIR=/var/lib/mattermost
  mkdir -p ${MATTERMOST_BASE_DIR}
  mkdir -pv ${MATTERMOST_BASE_DIR}/{data,logs,config,plugins,client-plugins}
  chown -R 2000:2000 ${MATTERMOST_BASE_DIR}
}

ask_database_host() {
  DB_HOST=$(whiptail --title "XiVO CC Multi Server Setup" --inputbox "The pgxivocc container was not found on this host. Please enter the pgxivocc database external address." 8 78 3>&1 1>&2 2>&3)
  DB_PORT="5443"
}

check_split_setup() {
  if is_monoserversetup; then
    # pgxivocc container is found on the host: do not ask where is the db
    return 0
  else
    # pgxivocc container not found on the host: ask where is the db
    ask_database_host
  fi
}

create_mm_db_in_pgxivocc() {
  # Run script with database host as argument
  local OPTION_HOST="-h $DB_HOST"
  if bash ${MM_DB_SCRIPT_PATH}${MM_DB_CREATION_SCRIPT} "$OPTION_HOST" -w "$MMUSER_DB_PASSWORD"; then
    echo "Mattermost database creation succeeded."
  else
    echo "Mattermost database creation failed."
    exit 100
  fi
}

pull_and_start_mm() {
  xivocc-dcomp pull
  xivocc-dcomp up -d
}

is_mattermost_running() {
  xivocc-dcomp exec -T mattermost mattermost-cli team list >/dev/null
  [ $? -eq 0 ]
}

waiting_for_mattermost() {
  local wait_time=60
  echo "It should not take more than a minute..."
  echo -n "Waiting for mattermost..."

  local mm_running="false"
  for _ in $(seq 1 $wait_time); do
    if is_mattermost_running; then
      mm_running="true"
      break
    else
      sleep 1
      echo -n "."
    fi
  done
  echo
  if [ "$mm_running" = "false" ]; then
    echo "ERROR: Mattermost is not running."
    exit 1
  fi
}

configure_mm() {
  local team_exists=$(
    xivocc-dcomp exec -T mattermost mattermost-cli team search "xivo" | grep -oP "^xivo: XiVO" >/dev/null
    echo $?
  )
  if [ $team_exists -ne 0 ]; then
    xivocc-dcomp exec -T mattermost mattermost-cli team create --name xivo --display_name "XiVO" >/dev/null
    echo -e "\e[1;33m...team created\e[0m"
  fi
  local user_exists=$(
    xivocc-dcomp exec -T mattermost mattermost-cli user search admin | grep -oP "^username: admin" >/dev/null
    echo $?
  )
  if [ $user_exists -ne 0 ]; then
    xivocc-dcomp exec -T mattermost mattermost-cli user create --system_admin --username admin --password "${MMADMIN_PASSWORD}" --email root@localhost >/dev/null
    echo -e "\e[1;33m...admin user created\e[0m"
    xivocc-dcomp exec -T mattermost mattermost-cli team add xivo admin >/dev/null
    echo -e "\e[1;33m...admin user added to team\e[0m"
  else
    echo -e "\e[1;32mNot updating admin user, it already exists in mattermost\e[0m"
  fi
}

create_admin_token() {
  local ADMIN_TOKEN
  set +e
  ADMIN_TOKEN=$(xivocc-dcomp exec -T mattermost create-admin-token)
  local EXIT_CODE=$?
  set -e
  if [ $EXIT_CODE -eq 0 ]; then
    set_custom_env_parameter "CHAT_SERVER_ADMIN_TOKEN" "$ADMIN_TOKEN"
  else
    echo -e "\e[1;31mError while creating admin token, command output follows\e[0m"
    echo "> ${ADMIN_TOKEN}"
    exit $EXIT_CODE
  fi
}

enable_xuc_integration() {
  set_custom_env_parameter "CHAT_SERVER_ENABLE" "true"
}

restart_services_if_needed() {
  xivocc-dcomp up -d
}

main() {
  echo -e "\e[1;33mMattermost chat backend configuration...\e[0m"
  if ! is_xivoucaddon; then
    check_split_setup
  fi
  set_mmuser_db_secret_custom_env_file
  setup_mm_environment
  echo -e "\e[1;33m...creating database\e[0m"
  create_mm_db_in_pgxivocc
  echo -e "\e[1;33m...starting mattermost\e[0m"
  pull_and_start_mm
  echo -e "\e[1;33m...waiting for mattermost to connect to database and start\e[0m"
  waiting_for_mattermost
  echo -e "\e[1;33m...creating mattermost base config\e[0m"
  configure_mm
  echo -e "\e[1;33m...creating admin token\e[0m"
  create_admin_token
  echo -e "\e[1;33m...enabling link with xuc\e[0m"
  enable_xuc_integration
  restart_services_if_needed

  echo -e "\e[1;33mMattermost chat backend installed and ready to be used.\e[0m"
}

main "${@}"

exit 0
